<?php

namespace App\Services\Info;

use App\Repositories\Info\InfoRepository;
use App\Repositories\Interfaces\InfoRepositoryInterface;
use App\Services\BaseService;
use Illuminate\Http\Request;
use GuzzleHttp\{Client, Exception\GuzzleException, Psr7\Request as ClientRequest};
use Telegram\Bot\Laravel\Facades\Telegram;

/**
 * Service for Info
 */
class InfoService extends BaseService
{
    // Incoming request
    private Request $request;

    // Repository for Info
    private InfoRepositoryInterface $infoRepository;

    // GuzzleHttp client example
    protected Client $client;

    /**
     * Class constructor
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        // Request initialization
        $this->request = $request;

        // InfoRepository initialization
        $this->infoRepository = new InfoRepository();

        $this->client = new Client();
    }

    /**
     * List of orders
     * @return Object
     */
    public function orders(): Array
    {
        try {

            $url =  config('app.base_kassa_telegram_api_url')  . 'info/order/1206';

            $request = new ClientRequest('GET', $url);

            $response =  $this->client->send($request, [
                'headers' => [
                    'Content-Type' => 'Content-Type: application/json',
                ],
                'connect_timeout' => 20,
                'verify' => false,
                'http_errors' => false,
            ]);

            if ($response) {
                $responseText = $response->getBody()->getContents();
                $responseCode = $response->getStatusCode();
                $result['content'] = $responseText;
                $result['code'] = $responseCode;
                $result['type'] = 'cancel';

                $tg = Telegram::getMe();
                $result['me'] = $tg;

                $updates = Telegram::getUpdates();
                $result['dollar'] = json_encode($updates);
            } else {
                $result['code'] = '500';
                $result['type'] = 'cancel';
            }

            return $this->result(['data' => $result]);

            
        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }
}