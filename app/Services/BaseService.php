<?php


namespace App\Services;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;


class BaseService
{
    protected function errValidate($message): array
    {
        return $this->error(422, $message);
    }

    protected function errFobidden($message): array
    {
        return $this->error(403, $message);
    }
    protected function errNotFound($message): array
    {
        return $this->error(404, $message);
    }

    protected function errService($message): array
    {
        return $this->error(500, $message);
    }

    protected function result($data)
    {
        $rData = $data;
        return $rData;
    }

    protected function error(int $code, string $message): array
    {
        return [
            'data' => [
                'message' => $message,
                'status' => 0
            ],
            'httpCode' => $code
        ];
    }

    protected function errorWithoutCode(): array
    {
        $rData = ['status' => 0];
        return [
            'data' => $rData,
            'httpCode' => 200
        ];
    }
}
