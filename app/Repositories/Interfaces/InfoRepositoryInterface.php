<?php

namespace App\Repositories\Interfaces;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Query\Builder;

interface InfoRepositoryInterface
{
    /**
     * Getting list of orders
     * @param Array $request
     * @return LengthAwarePaginator
     */
    public function orders(Array $request): LengthAwarePaginator;
}