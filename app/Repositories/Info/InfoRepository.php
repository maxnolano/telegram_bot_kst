<?php


namespace App\Repositories\Info;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Query\Builder;
use App\Repositories\Interfaces\InfoRepositoryInterface;
use GuzzleHttp\{Client, Exception\GuzzleException, Psr7\Request};

class InfoRepository implements InfoRepositoryInterface
{
    // GuzzleHttp client example
    protected Client $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * Getting list of orders
     * @param Array $request
     * @return LengthAwarePaginator
     */
    public function orders(Array $request): LengthAwarePaginator
    {
        
    }
}