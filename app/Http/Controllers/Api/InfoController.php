<?php

namespace App\Http\Controllers\Api;

use App\Traits\MakeComponents;
use App\Traits\RequestTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ApiController;
use App\Services\Info\InfoService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use PhpParser\JsonDecoder;
use GuzzleHttp\{Client, Exception\GuzzleException, Psr7\Request as ClientRequest};
use Telegram\Bot\Laravel\Facades\Telegram;

class InfoController extends ApiController
{
    use RequestTrait;
    use MakeComponents;

    /**
     * List of orders
     * @param Request $request
     * @return Object
     */
    public function orders(Request $request): Array {
        $response = (new InfoService($request))->orders();
        return $this->result($response);
    }

    public function webhook(){
        // return $this->apiRequest('setWebhook', ['url' => url(route('webhook')), ]) ? ['success'] : ['something wrong'];
        return $this->apiRequest('setWebhook', ['url' => url(route('webhook')), ]);
    }

    public function index(){
        $result = json_decode(file_get_contents('php://input'));

        $action = $result->message->text;
        $userId = $result->message->from->id;

        if($action == '/start'){
            $text = 'Please choose city that can see time';

            $option = [
                ['Tehran', 'Adelaide'],
                ['Istanbul', 'New York']
            ];

            $this->apiRequest('sendMessage', [
                'chat_id' => $userId,
                'text' => $text,
                'reply_markup' => $this->keyboardBtn($option)
            ]);
        }
    }

    public function setwebhook(Request $request){
        $result = $this->sendTelegramData('setwebhook', ['query' => ['url' => $request->url . '/' . Telegram::getAccessToken()]]);
        return $result;
    }

    public function getwebhookinfo(Request $request){
        $result = $this->sendTelegramData('getWebhookInfo');
        return $result;
    }

    public function sendTelegramData($route = '', $params = [], $method = 'POST'){
        $client = new \GuzzleHttp\Client( ['base_uri' => 'https://api.telegram.org/bot' . Telegram::getAccessToken() . '/']);
        $result = $client->request($method, $route, $params);
        return (string) $result->getBody();
    }
}