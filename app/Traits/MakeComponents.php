<?php

namespace App\Traits;

trait MakeComponents {
    
    private function keyboardBtn($option){
        $keyboard = [
            'keyboard' => $option,
            'resize_keyboard' => true,
            'one_time_keyboard' => false,
            'selectve' => true
        ];

        $keyboard = json_encode($keyboard);
        return $keyboard;
    }
}