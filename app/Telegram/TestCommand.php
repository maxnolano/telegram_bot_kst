<?php

namespace App\Telegram;

use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Laravel\Facades\Telegram;

class TestCommand extends Command {
    protected $name = 'test';

    protected $description = 'Test command, get a list of commands';

    public function handle(){

        $this->replyWithChatAction(['action' => Actions::TYPING]);

        $telegramUser = Telegram::getWebhookUpdates()['message'];

        $this->replyWithMessage(compact($telegramUser));
    }
}