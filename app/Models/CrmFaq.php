<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Orderable;

class CrmFaq extends Model
{
    use HasFactory;

    use Orderable;

    public $timestamps = false;

    protected $table = 'crm_faq';

    protected $fillable = [
        'id',
        'question',
        'answer',
        'disabled',
        'deleted',
        'lang'
    ];
}
