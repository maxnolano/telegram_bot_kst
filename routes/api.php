<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\InfoController;
use Telegram\Bot\Laravel\Facades\Telegram;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['namespace' => 'Api', 'prefix' => 'info'], function(){
    Route::get('/orders', [InfoController::class, 'orders']);
    Route::post('/setwebhook', [InfoController::class, 'setwebhook']);
    Route::post('/getwebhookinfo', [InfoController::class, 'getwebhookinfo']);
});

Route::any('/'.env('TELEGRAM_BOT_TOKEN'), [InfoController::class, 'index'])->name('webhook');