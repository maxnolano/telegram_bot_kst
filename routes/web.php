<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\InfoController;
use Telegram\Bot\Laravel\Facades\Telegram;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/webhook', [InfoController::class, 'webhook']);

Route::post(Telegram::getAccessToken(), function(){
    Telegram::commandsHandler(true);
});

